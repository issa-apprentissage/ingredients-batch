package fr.baldir.courses.et.recettes.batch;
// https://docs.spring.io/spring-batch/docs/current/reference/html/testing.html#testing

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringBootTest // TODO : fix pour éviter de charger tout et maitriser l'environnement de test
@SpringBatchTest
@SpringJUnitConfig
//        ({
//        BatchConfiguration.class,
//        BatchTestConfiguration.class,
//        BatchAutoConfiguration.class,
//        DataSourceAutoConfiguration.class,
//        DefaultBatchConfiguration.class,
//})
public class IngredientsBatchStepIT {

    // https://docs.spring.io/spring-batch/docs/current/reference/html/testing.html#endToEndTesting

    // On peut aussi tester juste des petits bouts (par ex Step)


    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Test
    public void testStep(@Autowired Job job) {
        this.jobLauncherTestUtils.setJob(job);

        JobExecution jobExecution = jobLauncherTestUtils.launchStep("step1");


        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
    }
}
