# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Testcontainers Postgres Module Reference Guide](https://www.testcontainers.org/modules/databases/postgres/)
* [Spring Batch](https://docs.spring.io/spring-boot/docs/2.7.8/reference/htmlsingle/#howto.batch)
* [Testcontainers](https://www.testcontainers.org/)
* [Spring Data JDBC](https://docs.spring.io/spring-boot/docs/2.7.8/reference/htmlsingle/#data.sql.jdbc)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.7.8/reference/htmlsingle/#using.devtools)
* [Spring Configuration Processor](https://docs.spring.io/spring-boot/docs/2.7.8/reference/htmlsingle/#appendix.configuration-metadata.annotation-processor)

### Guides
The following guides illustrate how to use some features concretely:

* [Creating a Batch Service](hfttps://spring.io/guides/gs/batch-processing/)
* [Using Spring Data JDBC](https://github.com/spring-projects/spring-data-examples/tree/master/jdbc/basics)

