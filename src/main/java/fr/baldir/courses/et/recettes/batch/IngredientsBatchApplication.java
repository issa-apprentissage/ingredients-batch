package fr.baldir.courses.et.recettes.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngredientsBatchApplication {

    public static void main(String[] args) throws Exception {
        System.exit(
                SpringApplication.exit(
                        SpringApplication.run(IngredientsBatchApplication.class, args)
                )
        );
    }


}
