package fr.baldir.courses.et.recettes.batch;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonItemProcessorTest {

    @Test
    void dfgfg() throws Exception {
        PersonItemProcessor personItemProcessor = new PersonItemProcessor();
        Person process = personItemProcessor.process(new Person("issa", "maréga"));
        assertThat(process.getFirstName()).isEqualTo("ISSA");
        assertThat(process.getLastName()).isEqualTo("MARÉGA");
    }

}