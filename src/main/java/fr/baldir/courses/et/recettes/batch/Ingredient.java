package fr.baldir.courses.et.recettes.batch;

/**
 * @param id            correspond à alim_code
 * @param nomIngredient correspond à ALIM_NOM_INDEX_FR
 */
public record Ingredient(Integer id, String nomIngredient) {

}
